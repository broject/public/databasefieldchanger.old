Vue.component('broject-databasefieldchanger-string', require('./components/types/String.vue').default);
Vue.component('broject-databasefieldchanger-textarea', require('./components/types/Textarea.vue').default);
Vue.component('broject-databasefieldchanger-checkbox', require('./components/types/Checkbox.vue').default);
Vue.component('broject-databasefieldchanger-select', require('./components/types/Select.vue').default);
